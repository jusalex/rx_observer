package com.epam.rx.observer;

public class Application {
    public static void main(String[] args) throws Exception {
        ObserverHandler observerHandler = new ObserverHandler();
        WordsWorker wordsWorker = new WordsWorker("text.txt", observerHandler.getObservable());
        wordsWorker.doWork();
    }
}
