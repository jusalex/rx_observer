package com.epam.rx.observer.subscribers;

import rx.Subscriber;

public class LongestWordSubscriber extends Subscriber<String> {

    private String longestWord = "";

    @Override
    public void onCompleted() {
        System.out.println("The longest word is \"" + longestWord + "\"");
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onNext(String o) {
        if (o.length() > longestWord.length()) {
            longestWord = o;
        }
    }

    public String getLongestWord() {
        return longestWord;
    }
}
