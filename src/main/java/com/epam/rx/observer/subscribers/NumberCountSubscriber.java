package com.epam.rx.observer.subscribers;

import java.util.concurrent.atomic.AtomicLong;

import rx.Subscriber;

public class NumberCountSubscriber extends Subscriber<String> {

    private AtomicLong counter = new AtomicLong(0);

    @Override
    public void onCompleted() {
        System.out.println("Total numbers count: " + counter.get());
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onNext(String o) {
        try {
            Long.parseLong(o);
            counter.incrementAndGet();
        } catch (NumberFormatException e) {

        }
    }

    public long getCount() {
        return counter.longValue();
    }
}
