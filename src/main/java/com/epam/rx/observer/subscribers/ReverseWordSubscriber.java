package com.epam.rx.observer.subscribers;

import rx.Subscriber;

public class ReverseWordSubscriber extends Subscriber<String> {

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onNext(String word) {
        System.out.println(new StringBuilder(word).reverse().toString());
    }
}
