package com.epam.rx.observer.subscribers;

import java.util.concurrent.atomic.AtomicLong;

import rx.Subscriber;

public class WordCountSubscriber extends Subscriber<String> {

    private AtomicLong counter = new AtomicLong(0);

    @Override
    public void onCompleted() {
        System.out.println("Total words count: " + counter.get());
    }

    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onNext(String o) {
        counter.incrementAndGet();
    }

    public long getCount() {
        return counter.get();
    }
}
