package com.epam.rx.observer;

import com.epam.rx.observer.subscribers.LongestWordSubscriber;
import com.epam.rx.observer.subscribers.NumberCountSubscriber;
import com.epam.rx.observer.subscribers.ReverseWordSubscriber;
import com.epam.rx.observer.subscribers.WordCountSubscriber;

import rx.subjects.PublishSubject;

public class ObserverHandler {

    private PublishSubject<String> observable = PublishSubject.create();

    public ObserverHandler() {
        initSubscribers();
    }

    private void initSubscribers() {
        observable.subscribe(new ReverseWordSubscriber());
        observable.subscribe(new WordCountSubscriber());
        observable.subscribe(new NumberCountSubscriber());
        observable.subscribe(new LongestWordSubscriber());
    }

    public PublishSubject<String> getObservable() {
        return observable;
    }
}
