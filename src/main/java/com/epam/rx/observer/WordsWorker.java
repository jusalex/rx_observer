package com.epam.rx.observer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import rx.subjects.PublishSubject;

public class WordsWorker {

    private PublishSubject<String> observable;
    private File file;
    private ClassLoader classLoader = getClass().getClassLoader();

    public WordsWorker(String filePath, PublishSubject<String> observable) {

        this.observable = observable;
        this.file = new File(classLoader.getResource(filePath).getFile());
    }

    public void doWork() throws IOException {
        try (FileReader fileReader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(fileReader);
            String line;
            while ((line = br.readLine()) != null) {
                String[] words = line.trim().replaceAll(",|\\.|\\?|!", "").split(" ");
                for (String word : words) {
                    word = word.trim();
                    if (word.length() > 0) {
                        observable.onNext(word);
                    }
                }
            }
        }
        observable.onCompleted();
    }
}
