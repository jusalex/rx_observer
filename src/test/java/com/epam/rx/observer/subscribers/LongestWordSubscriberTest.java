package com.epam.rx.observer.subscribers;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class LongestWordSubscriberTest {

    @Test
    public void shouldReturnRightWordCount() {
//        given
        LongestWordSubscriber subscriber = new LongestWordSubscriber();

//        when
        subscriber.onNext("1");
        subscriber.onNext("22");
        subscriber.onNext("333");

//        then
        assertThat(subscriber.getLongestWord(), is("333"));
    }
}
