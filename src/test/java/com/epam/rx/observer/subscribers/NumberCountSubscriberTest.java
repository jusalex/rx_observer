package com.epam.rx.observer.subscribers;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class NumberCountSubscriberTest {

    @Test
    public void shouldReturnRightWordCount() {
//        given
        NumberCountSubscriber subscriber = new NumberCountSubscriber();

//        when
        subscriber.onNext("1");
        subscriber.onNext("22");
        subscriber.onNext("333");
        subscriber.onNext("a");
        subscriber.onNext("bb");

//        then
        assertThat(subscriber.getCount(), is(3L));
    }
}
