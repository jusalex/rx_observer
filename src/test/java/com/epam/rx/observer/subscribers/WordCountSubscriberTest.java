package com.epam.rx.observer.subscribers;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class WordCountSubscriberTest {

    @Test
    public void shouldReturnRightWordCount() {
//        given
        WordCountSubscriber wordCountSubscriber = new WordCountSubscriber();

//        when
        wordCountSubscriber.onNext("1");
        wordCountSubscriber.onNext("2");
        wordCountSubscriber.onNext("3");

//        then
        assertThat(wordCountSubscriber.getCount(), is(3L));
    }
}
